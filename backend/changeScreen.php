<?php
/**
 * For license information; see license.txt
 * @author frankhouweling
 * @date 30-09-14
 */

$i = 0;
$dir = '../interfaces/';
if ($handle = opendir($dir)) {
    while (($file = readdir($handle)) !== false){
        if (!in_array($file, array('.', '..')) && !is_dir($dir.$file))
            $i++;
    }
}
$amountOfFiles = $i;

if( isset( $_POST['interface'] ) ){
    file_put_contents("interface.txt", $_POST['interface']);
}

if( isset( $_GET['interface'] ) ){
    file_put_contents("interface.txt", $_GET['interface']);
header( "Location: changeScreen.php" );
}

?>
<html>
<head>
    <meta name="viewport" content="width=300, initial-scale=1.2">
    <style type="text/css">
        input{
            padding: 10px;
            font-size: 24px;
            margin: 10px;
        }
    </style>
</head>
    <body>
        <form method="post">
            <h3>Ga naar scherm:</h3>
            <?php

            for($p=1;$p<=$amountOfFiles;$p++){
                ?>
                <input type="submit" name="interface" value="<?php echo $p; ?>" /><br />
                <?
            }

            ?>


            <br />
        </form>

<a href="changeScreen.php?interface=<?php echo ( file_get_contents("interface.txt") + 1); ?>">Volgende scherm</a>
<br /><br />
<a href="changeScreen.php?interface=<?php echo ( file_get_contents("interface.txt") - 1); ?>">Vorige scherm</a>
    </body>

</html>